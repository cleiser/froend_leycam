angular.module('app').controller('ScrapingCtrl', function (toastr, CONEX) {

    var self = this;
    self.obj = {};

    self.consultar = function(dni){
    	self.obj = {};
    	CONEX('comun','scraping/dnipadron').GET({dni: dni}).$promise.then(function (r) {
            self.obj = r;
        }, function (err) {
        });
    };    

});