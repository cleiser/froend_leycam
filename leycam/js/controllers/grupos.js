angular.module('app').controller('GruposCtrl', 
	function (toastr, CONEX, $mdDialog) {

    var self = this;

    self.listar = function(search){
    	var filtro = {
    		search: search
    	};
    	CONEX('comun','grupos').GET_LIST(filtro).$promise.then(function (r) {
            self.listado = r;
        }, function (err) {
        });
    };

    self.listar();

    self.new_edit = function(ev, obj) {
	    $mdDialog.show({
	      controller: 'GruposFormCtrl',
	      controllerAs: "form",
	      templateUrl: 'views/grupos/form.html',
	      parent: angular.element(document.body),
	      targetEvent: ev,
	      clickOutsideToClose:false,
	      fullscreen: false, // Only for -xs, -sm breakpoints.
	      locals: {obj: obj},
	    })
	    .then(function(obj) {
	       self.listar();
	    }, function() {
	      
	    });
	};

	self.delete = function(ev, obj) {
	    // Appending dialog to document.body to cover sidenav in docs app
	    var confirm = $mdDialog.confirm()
	          .title('¿Desea eliminar el grupo: '+obj['nombre']+'?')
	          .textContent('Mira, si eliminas ya no podrás recuperar este grupo')
	          .ariaLabel('a')
	          .targetEvent(ev)
	          .ok('SI, ESTOY SEGURO')
	          .cancel('NO QUIERO');

	    $mdDialog.show(confirm).then(function() {
	      	CONEX('comun','grupos/delete').DELETE({id: obj['id']}).$promise.then(function (r) {
	            self.listar();
	        }, function (err) {
	        	toastr.error(err.data, 'No hemos podido eliminar');
	        });
	    }, function() {
	      	toastr.info('Ok, no eliminaste');
	    });
	};

});

angular.module('app').controller('GruposFormCtrl', 
	function (toastr, CONEX, $mdDialog, obj) {
	var self = this;
	self.obj = {};
	self.tiposgrupo = [];

	if(obj){
		CONEX('comun','grupos/edit').GET({id: obj['id']}).$promise.then(function (r) {
            self.obj = r;
        }, function (err) {
        });
	}

	self.cancel = function(){
		$mdDialog.cancel();
	};

	self.lis_tipos = function(){
    	CONEX('comun','tiposgrupo/searchform').GET_LIST().$promise.then(function (r) {
            self.tiposgrupo = r;
        }, function (err) {
        });
    };

    self.lis_tipos();


    self.guardar = function(){

    	if(self.obj['id']){
    		CONEX('comun','grupos/update').PUT(self.obj).$promise.then(function (r) {
	            toastr.success("Grupo actualizado correctamente");
	            $mdDialog.hide(r);
	        }, function (err) {
	        	toastr.error(err.data, "Error al actualizar");
	        });
    	}else{
	    	self.obj['estado'] = '1';
	    	CONEX('comun','grupos/add').POST(self.obj).$promise.then(function (r) {
	            toastr.success("Grupo creado correctamente");
	            $mdDialog.hide(r);
	        }, function (err) {
	        	toastr.error(err.data, "Error al registrar");
	        });
	    }
    };

});