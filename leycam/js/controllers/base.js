angular.module('app').controller('BaseCtrl', function ($scope,
        $state, $mdSidenav, $timeout, $rootScope, data_app) {

    $scope.$on('ocLazyLoad.moduleLoaded', function (e, params) {
    });

    $scope.$on('ocLazyLoad.componentLoaded', function (e, params) {
    });

    $scope.$on('ocLazyLoad.fileLoaded', function (e, file) {
    });
    $scope.app = data_app;
    $rootScope.state = $state;
    $rootScope.Indeterminado = true;

    function openPage() {
        $scope.closeAside();
    }

    $scope.goBack = function () {
        $window.history.back();
    };

    $scope.openAside = function () {
        $timeout(function () {
            $mdSidenav('menu').open();
        });
    };
    $scope.closeAside = function () {
        $timeout(function () {
            $document.find('#menu').length && $mdSidenav('menu').close();
        });
    };

    $scope.menu_folder = function () {
        if ($scope.app.setting.menuFolded) {
            $scope.app.setting.menuFolded = false;
        } else {
            $scope.app.setting.menuFolded = true;
        }
    };

    $scope.aside_folder = function () {
        if ($scope.app.setting.asideFolded) {
            $scope.app.setting.asideFolded = false;
        } else {
            $scope.app.setting.asideFolded = true;
        }
    };

    $scope.AbrirToogle = correrToogle('sid_config_app');

    function correrToogle(navID) {
        return function () {
            $mdSidenav(navID)
            .toggle()
            .then(function () {
            });
        };
    }
    $scope.close_sidenav = function (idNav) {
        $mdSidenav(idNav).close()
        .then(function () {
        });
    };
    angular.module('cardDemo2', ['ngMaterial'])

    .controller('AppCtrl', function($scope) {
      $scope.imagePath = 'img/washedout.png';
    });

});

angular.module('app').controller('MenuCtrl', function ($scope, $mdSidenav, data_app) {

    $scope.menu = [
                    {
                        "state":"web.1",
                        "title":"SALES SYSTEM","type":"link"
                    },
                    {
                        "children":[
                                {
                                    "icon":"person",
                                    "state":"web.bienvenida",
                                    "title":"Bienvenida del Adm",
                                    "type":"link"
                                }
                            ]
                    },
                    {
                        "state":"web.2",
                        "title":"MIS VENTAS","type":"link"
                    },
                    {
                        "children":[

                                {
                                    "icon":"group",
                                    "state":"web.grupos",
                                    "title":"Grupos",
                                    "type":"link"
                                },
                                {
                                    "icon":"group",
                                    "state":"web.personas",
                                    "title":"Personas",
                                    "type":"link"
                                },
                                {
                                    "icon":"home",
                                    "state":"web.cliente",
                                    "title":"Clientes",
                                    "type":"link"
                                },
                                {
                                    "icon":"home",
                                    "state":"web.venta",
                                    "title":"Venta",
                                    "type":"link"
                                },
                                {
                                    "icon":"toc",
                                    "state":"web.grupos",
                                    "title":"Productos",
                                    "type":"link"
                                },
                                {

                                    "icon":"home",
                                    "state":"web.lote",
                                    "title":"Lote",
                                    "type":"link"
                                },

                            ]
                    },

                    ];
    
    $scope.app = data_app;
    $scope.close_sidenav = function (idNav) {
        $mdSidenav(idNav).close()
        .then(function () {
        });
    };

});
