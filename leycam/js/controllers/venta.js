angular.module('app').controller('VentaCtrl', function (toastr, CONEX, $mdDialog) {

    var self = this;

    self.listar = function(search){

        var filtro = {
        
            search: search
           
        };
        CONEX('comun','venta').GET_LIST(filtro).$promise.then(function (r) {
            self.listado = r;
            
        }, function (err) {
        });

    };

    self.listar();

     self.new_edit = function(ev) {
    $mdDialog.show({
      controller: 'VentaFormCtrl',
      controllerAs: "form",
      templateUrl: 'views/venta/form.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:false,
      fullscreen: false // Only for -xs, -sm breakpoints.
    })
    .then(function(answer) {
      
    }, function() {
        toastr.succes("hola mundo");
      
    });
  };



});

angular.module('app').controller('VentaFormCtrl', 
    function (toastr, CONEX, $mdDialog) {
    var self = this;

    self.cancel = function(){
        $mdDialog.cancel();


    };
});