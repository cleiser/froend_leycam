angular.module('app').controller('MusicaCtrl', function (toastr, CONEXF) {

    var self = this;

    toastr.success("Hola");

    self.search_musica = function(search){
    	var filtro = {
    		pagina: 0,
    		search: search,
    		tipo: "TEMA"
    	};
    	CONEXF('melocoton','reproductor/search_musica.php').POST(filtro).$promise.then(function (r) {
            self.listado = r['listado'];
            self.pagination = r['pagination'];
            self.cargado = true;
        }, function (err) {
        });
    };

    self.search_musica();


});