app.config(function ($stateProvider, $locationProvider, $urlRouterProvider, 
    $ocLazyLoadProvider) {
    $locationProvider.hashPrefix('');
    $urlRouterProvider.otherwise("/web");
    $stateProvider
            .state('web', {
                url: '/web',
                data: {page: 'Multiventas LeyCam'},
                views: {
                    '': {
                        templateUrl: 'views/base.html',
                        controller:'BaseCtrl',
                    },
                    'menu@web': {
                        templateUrl: 'views/menu.html',
                        controller: 'MenuCtrl'
                    },
                    'barra@web': {
                        templateUrl: 'views/barra.html',
                    }
                },
                resolve: {
                    loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load('js/controllers/base.js');
                        }]
                }
            })
            .state("web.bienvenida", {
                url: "/bienvenida",
                data: {section: 'Web', page: 'Bienvenida del Administrador'},
                templateUrl: "views/bienvenida.html",
            })
            .state("web.musica", {
                url: "/musica",
                data: {section: 'App', page: 'Buscar música'},
                templateUrl: "views/musica/index.html",
                controller: 'MusicaCtrl',
                controllerAs: "ctrl",
                resolve: {
                    loadMusica: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/musica.js');
                    }]
                }
            })
            .state("web.grupos", {
                url: "/grupos",
                data: {section: 'App', page: 'Grupos'},
                templateUrl: "views/grupos/index.html",
                controller: 'GruposCtrl',
                controllerAs: "ctrl",
                resolve: {
                    loadMusica: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/grupos.js');
                    }]
                }
            })
            .state("web.personas", {
                url: "/personas",
                data: {section: 'App', page: 'Personas'},
                templateUrl: "views/personas/index.html",
                controller: 'PersonasCtrl',
                controllerAs: "ctrl",
                resolve: {
                    loadMusica: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/personas.js');
                    }]
                }
            })
            .state("web.cliente", {
                url: "/cliente",
                data: {section: 'App', page: 'Cliente'},
                templateUrl: "views/cliente/index.html",
                controller: 'ClienteCtrl',
                controllerAs: "ctrl",
                resolve: {
                    loadMusica: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/cliente.js');
                    }]
                }
            })
            .state("web.venta", {
                url: "/venta",
                data: {section: 'App', page: 'Venta'},
                templateUrl: "views/venta/index.html",
                controller: 'VentaCtrl',
                controllerAs: "ctrl",
                resolve: {
                    loadMusica: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/venta.js');
                    }]
                }
            })
            .state("web.lote", {
                url: "/lote",
                data: {section: 'App', page: 'Lote'},
                templateUrl: "views/lote/index.html",
                controller: 'LoteCtrl',
                controllerAs: "ctrl",
                resolve: {
                    loadMusica: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/lote.js');
                    }]
                }
            })
            .state("web.dnipadron", {
                url: "/dnipadron",
                data: {section: 'App', page: 'Grupos'},
                templateUrl: "views/scraping/dnipadron.html",
                controller: 'ScrapingCtrl',
                controllerAs: "ctrl",
                resolve: {
                    loadMusica: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load('js/controllers/scraping.js');
                    }]
                }
            });


      $locationProvider.html5Mode(false);
});
