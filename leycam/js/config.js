
var data_app = {
    name: 'LP',
    setting: {
        asideFolded: false
    }
};

app.value('data_app', data_app);

var services = {
    api_melocoton: 'http://misionlive.com/backend/melocoton/',
    api_comun: 'http://localhost:8000/api/comun/',

};

app.value('services', services).run(run_index);

function run_index($rootScope, $state, $q, $http) {
    $http.defaults.xsrfHeaderName = 'X-CSRFToken';
    $http.defaults.xsrfCookieName = 'csrftoken';
    $rootScope.tokenSocket = $q.defer();
    $rootScope.$userPromise = $q.defer();
    $rootScope.$state = $state;
};

app.config(function ($resourceProvider, $httpProvider) {

    $httpProvider.useApplyAsync(true);
    $resourceProvider.defaults.stripTrailingSlashes = false;
    $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';

    $resourceProvider.defaults.actions = {
        GET: {method: 'GET', isArray: false},
        GET_LIST: {method: 'GET', isArray: true},
        POST: {method: 'POST'},
        POST_LIST: {method: 'POST', isArray: true},
        PUT: { method: 'PUT' },
        PUT_LIST: { method: 'PUT', isArray: true},
        DELETE: { method: 'DELETE' },
        DELETE_LIST: { method: 'DELETE', isArray: true},
    };
});

app.factory('CONEX', function ($resource, services) {
    return function (app, recurso) {
        return $resource(services['api_'+app] + recurso+'/');
    };
});

app.factory('CONEXF', function ($resource, services) {
    return function (app, recurso) {
        return $resource(services['api_'+app] + recurso);
    };
});