
app.config(function ($mdThemingProvider) {

    $mdThemingProvider.theme('Blue')
            .primaryPalette('blue');

    $mdThemingProvider.theme('Teal')
            .primaryPalette('teal');

    $mdThemingProvider.theme('Blue')
            .primaryPalette('blue');

    $mdThemingProvider.setDefaultTheme('Blue');
    $mdThemingProvider.alwaysWatchTheme(true);
});
